//
//  FirstViewController.swift
//  Antenna Point
//
//  Created by Timothy Aven on 10/5/16.
//  Copyright © 2016 Antennas Direct. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    static let sharedInstance = MapViewController()
    private let userData = UserData.sharedInstance
    
    //Connection to mapView
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var vectorSelectorBtn: UIButton!
    @IBOutlet var pointLabel: UILabel!
    
    //Local Variables
    let locationManager = CLLocationManager()
    let dataManager = DataManager.sharedInstance
    var jsonTowerDictionary = [String: AnyObject]()
    var alertController = UIAlertController()
    var longAndLatDictionary = [CLLocationCoordinate2D]()
    var testCoordinate = CLLocation()
    var directionPointDegrees = Double()
    var coordinate = CLLocationCoordinate2D()
    var userHeading = CLLocationDirection()
    var userLocationAnnotationView = MKAnnotationView()
    var mapCamera = MKMapCamera()
    var vectorOverlayTracker = Bool()
    var finalBearing = Double()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Assigns the AntennaPoint title image to top of nav bar
        let titleImage = UIImage(named: "AntennaPoint Logo")
        let titleImageView = UIImageView(image: titleImage)
        self.navigationItem.titleView = titleImageView
     
        //Setting map view delegate and mapView properties
        self.mapView.delegate = self
        self.mapView.isPitchEnabled = false
        self.mapView.isRotateEnabled = false
        self.mapView.isScrollEnabled = false
        self.mapView.isZoomEnabled = true
        self.mapView.showsCompass = true
        self.vectorOverlayTracker = false
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //These Process are housed here due to how the onboarding works, if used in the viewDidLoad will cause the app to crash because it will be trying to use location information before the user has chosen to allow use of their location.
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            self.locationManager.delegate = self
            if self.mapView.annotations.count <= 0 {
                self.sendHTTPRequest()
                self.circleOverlayCreator(radius: 111120)
                self.circleOverlayCreator(radius: 92600)
                self.circleOverlayCreator(radius: 64820)
                self.locationManager.delegate = self
                self.locationManager.distanceFilter = kCLDistanceFilterNone
                self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
                self.locationManager.startUpdatingLocation()
                self.locationManager.startUpdatingHeading()
                
                self.vectorSelectorBtn.isEnabled = true
                
                self.coordinate = (self.locationManager.location?.coordinate)!
                self.userData.saveUserLocationAccess(complete: true)
              
            }
        } else {
            self.vectorSelectorBtn.isEnabled = false 
            self.alertController = UIAlertController(title: "Cannot Access User Location", message: "For Antenna Point to work it must be able to access your location. To allow access, on your phone's home screen go to SETTINGS -> ANTENNA POINT -> LOCATION and select 'While Using The App'", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { action in
                if(self.locationManager.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization))) {
                    self.locationManager.requestWhenInUseAuthorization()
                }
            }
            alertController.addAction(okAction)
            if self.alertController.actions.count == 1 {
                self.present(self.alertController, animated: true, completion:nil)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.locationManager.stopUpdatingHeading()
        self.locationManager.stopUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.userData.retreiveUserLocationAccess() == true {
            self.locationManager.startUpdatingHeading()
            self.locationManager.startUpdatingLocation()
        }
    }

    //Sends the HTTP Request for the tower data
    func sendHTTPRequest() {
       
        //Take the users current location coordiates and turn it into a string for HTTP request
        let userLongNum = NSNumber(value:(self.locationManager.location?.coordinate.longitude)! as Double)
        let userLong: String = userLongNum.stringValue
        let userLatNum = NSNumber(value:(self.locationManager.location?.coordinate.latitude)! as Double)
        let userLat: String = userLatNum.stringValue
        
        self.dataManager.HTTPRequestForJSON(userLong: userLong, userLat: userLat) {
            (parsedJSON: [String:AnyObject]) in
            self.jsonTowerDictionary = parsedJSON
            print(parsedJSON)
            self.setUpCustomUserLocationAnnotation()
            self.setUpTowerAnnotations()
        }
    }
    
    //Set the desired default view region of the map
    func setMapViewRegeion() {
        let region: MKCoordinateRegion = MKCoordinateRegionMakeWithDistance((self.locationManager.location?.coordinate)!, 220000, 220000)
        self.mapView.setRegion(region, animated: true)
    }
 
    //Vector Overlay button functionality
    @IBAction func vectorSelectorBtnPressed(sender: UIButton) {
        
        //Creates the overlay image if it is not already there
        if self.vectorOverlayTracker != true {
            self.locationManager.stopUpdatingHeading()
            self.calcBestAntennaDirection()
            let overlay = VectorOverlay(mapView: self.mapView, userCoordinate: self.locationManager.location!)
            self.mapView.add(overlay)
            self.vectorOverlayTracker = true
            self.locationManager.startUpdatingHeading()
        }
        
        //Removes current overlay image if it is already there
        else {
            self.mapView.remove(self.mapView.overlays.last!)            
            self.vectorOverlayTracker = false
        }
    }
    
    //Sets custom overlay for users location
    func setUpCustomUserLocationAnnotation() {
        let coordinate: CLLocationCoordinate2D = self.locationManager.location!.coordinate
        let userLocationAnnotation = UserLocationAnnoation(coordinate: coordinate)
        self.mapView.addAnnotation(userLocationAnnotation)
    }
    
    //Adds tower annotations to the map view 
    func setUpTowerAnnotations() {
        
        var annotationsDictionary = [Annotation]()
  
        if let nestedDictionary = self.jsonTowerDictionary["Towers"] as? [[String:AnyObject]] {
           
            for tower in nestedDictionary {
                let long = tower["longitude"] as! Double
                let lat = tower["latitude"] as! Double
                let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                
                self.longAndLatDictionary.append(coordinate)
                
                let annotation = Annotation(title: tower["description"] as! String, description: tower["name"] as! String, coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long))
                annotationsDictionary.append(annotation)
            }
            for annotation in annotationsDictionary {
                self.mapView.addAnnotation(annotation)
            }
        }
    }

    //Allows the app to have user calibrate while inside the map section
    func locationManagerShouldDisplayHeadingCalibration(_ manager: CLLocationManager) -> Bool {
        return true
    }
    
    //Keeps track of users current heading and any changes
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        if newHeading.headingAccuracy < 0 {return}
        let heading = newHeading.trueHeading > 0 ? newHeading.trueHeading : newHeading.magneticHeading
        self.userHeading = heading
        self.updateHeadingRotation()
    }
 
    //Updates the Cutoms User Location Annotation based on current heading information
    func updateHeadingRotation() {
        let rotation = CGFloat(self.userHeading/180 * .pi)
        self.mapCamera.centerCoordinate = (self.locationManager.location?.coordinate)!
        self.mapCamera.altitude = 600000
        self.mapCamera.heading = self.userHeading
        self.mapView.setCamera(self.mapCamera, animated: false)
    }

    //Sets the zoom level of the map
    func drawMapRect(mapRect: MKMapRect, zoomScale: MKZoomScale, inContext context: CGContext!) {
        let region: MKCoordinateRegion = MKCoordinateRegionMakeWithDistance((self.locationManager.location?.coordinate)!,200000, 200000)
        self.mapView.setRegion(region, animated: false)
    }
    
    //Sets up Annotations views
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseID = "reuse"
        var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)

        //Process for custom user location annotation
        if annotation is UserLocationAnnoation {
            self.userLocationAnnotationView = MKAnnotationView(annotation: annotationView as! MKAnnotation?, reuseIdentifier: reuseID)
            self.userLocationAnnotationView.canShowCallout = false
            userLocationAnnotationView.image = UIImage(named: "UserLocationAnnotation_CorrectSize")
            return userLocationAnnotationView
        }
        
        //Process for standard Map Pin annotations
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotationView as! MKAnnotation?, reuseIdentifier: reuseID)
            annotationView?.image = UIImage(named: "antenna")
            annotationView?.canShowCallout = true
            annotationView?.bringSubview(toFront: self.mapView)
        } else {
            annotationView?.annotation = annotation
        }
        return annotationView
    }

    //Created overlayes distance cirlcles
    func circleOverlayCreator(radius: CLLocationDistance) {
        let circle = MKCircle(center: (self.locationManager.location?.coordinate)!, radius: radius as CLLocationDistance)
        self.mapView.add(circle)
    }
    
    //Averages distances from each tower, finds best area to direct vector overaly
    func calcBestAntennaDirection() {
        
        var bestLocation = CLLocationCoordinate2D()
        var bestLocationAverage: Double = 0
        var tempDistanceArray = [CLLocationDistance]()
        
        for comparativeLocation in self.longAndLatDictionary {
            for location in self.longAndLatDictionary {
                let distance = self.distanceCalculator(locA: comparativeLocation, locB: location)
                if distance != 0 {
                    tempDistanceArray.append(distance)
                }
            }
            let distanceCount: Double = Double(tempDistanceArray.count)
            var totalDistanceForAverage: Double = 0
            for distance in tempDistanceArray {
                totalDistanceForAverage = totalDistanceForAverage + distance
            }
            let finalAverageDistance = totalDistanceForAverage / distanceCount
            if finalAverageDistance < bestLocationAverage || bestLocationAverage == 0 {
                bestLocationAverage = finalAverageDistance
                bestLocation = comparativeLocation
            }
        }
        self.finalBearing = self.bearingCalcualtor(location: bestLocation)
    }
    
    //Will be used for Distance between towers
    func distanceCalculator(locA: CLLocationCoordinate2D, locB: CLLocationCoordinate2D) -> CLLocationDistance {
        let locationA = CLLocation(latitude: locA.latitude, longitude: locA.longitude)
        let locationB = CLLocation(latitude: locB.latitude, longitude: locB.longitude)
        let distance: CLLocationDistance = (locationA.distance(from: locationB))
        return distance
    }
    
    //Determines where to face the overlayed vector image
    func bearingCalcualtor(location: CLLocationCoordinate2D) -> Double {
        let lat = location.latitude
        let lon = location.longitude
        let userMapPoint = MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: (self.locationManager.location?.coordinate.latitude)!, longitude: (self.locationManager.location?.coordinate.longitude)!))
        let destPoint = MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: lat, longitude: lon))
        let x = destPoint.x - userMapPoint.x
        let y = destPoint.y - userMapPoint.y
        let bearing = radiansToDegrees(radians: atan2(y, x)).truncatingRemainder(dividingBy: 360) + 90
        
        return bearing
        
    }
    
    //Radians -> Degrees
    private func radiansToDegrees(radians: Double) -> Double {
        return radians * 180 / .pi
    }
    
    //Degrees -> Radians
    private func degreesToRadians(degrees: Double) -> Double {
        return degrees * .pi / 180
    }
 
    //Handles the drawing of the circle overlays upon the map
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is VectorOverlay {
            let rotatedOverlayImage = UIImage(named: "VectorImageFinalSize")?.rotated(by: Measurement(value: self.finalBearing, unit: .degrees))
            let overlayView = VectorOverlayRenderer(overlay: overlay, overlayImage: rotatedOverlayImage!)
            
            return overlayView
        }
        else {
            let circleRenderer = MKCircleRenderer(circle: overlay as! MKCircle)
            circleRenderer.fillColor = UIColor.clear
            circleRenderer.alpha = 0.3
            if let circle: MKCircle = overlay as? MKCircle {
                
                switch circle.radius {
                case 64820:
                    circleRenderer.fillColor = UIColor.lightGray
                case 92600:
                    circleRenderer.fillColor = UIColor.yellow
                case 111120:
                    circleRenderer.fillColor = UIColor.red
                default:
                    circleRenderer.fillColor = UIColor.clear
                }
            }
            circleRenderer.strokeColor = UIColor.black
            
            return circleRenderer
        }
    }
}


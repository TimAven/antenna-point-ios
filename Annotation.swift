//
//  Annotations.swift
//  Antenna Point
//
//  Created by Timothy Aven on 11/1/16.
//  Copyright © 2016 Antennas Direct. All rights reserved.
//

import Foundation
import MapKit

//Map Annotations class
class Annotation: NSObject, MKAnnotation {
    
    var title: String?
    var subtitle: String?
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, description: String, coordinate: CLLocationCoordinate2D) {
    
        self.title = title
        self.subtitle = description
        self.coordinate = coordinate
       
        super.init()
    }
}

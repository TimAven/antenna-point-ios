//
//  TabBarController.swift
//  Antenna Point
//
//  Created by Timothy Aven on 10/17/16.
//  Copyright © 2016 Antennas Direct. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    private let userData = UserData.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        //Add back for pre or post login checking 
        
        if self.userData.retreiveUserInitialLoginStatus() == false {
            self.selectedIndex = 0
        } else {
            self.selectedIndex = 1
        }
 
        NSLog("\(self.selectedIndex)")
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

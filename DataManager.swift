//
//  DataManager.swift
//  Antenna Point
//
//  Created by Timothy Aven on 12/7/16.
//  Copyright © 2016 Antennas Direct. All rights reserved.
//

import Foundation

class DataManager: NSObject {
    static let sharedInstance = DataManager()
    var jsonResults = [String:AnyObject]()
    
    
    func HTTPRequestForJSON(userLong: String, userLat: String, completionHandler: @escaping ([String:AnyObject]) -> Void){

        //This CANNOT CHANGE
        let URL = NSURL(string: "https://www.antennasdirect.com/apapp/?lat=\(userLat)&lon=\(userLong)")
        
        //Send HTTP Request
        let task = URLSession.shared.dataTask(with: URL as! URL) {(data, response, error) in
           
            //Parse the received JSON
            if error == nil && data != nil {
              
                do {
                    self.jsonResults = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String:AnyObject]
                    completionHandler(self.jsonResults)
             
                } catch {
                    print("ERROR LOADING JSON")
                }
            }
        }
        task.resume()
    }
    
}

//
//  HelpViewController.swift
//  Antenna Point
//
//  Created by Timothy Aven on 10/17/16.
//  Copyright © 2016 Antennas Direct. All rights reserved.
//

import UIKit
import CoreLocation


class HelpViewController: UIViewController, CLLocationManagerDelegate {

    private let userData = UserData.sharedInstance
    private var userInitialLogin: Bool?
    private let mapViewSharedInstance = MapViewController.sharedInstance
    private var pages: [OnboardingContentViewController]! = []
    private let deviceChecker = UIDevice.current.model
    
    //Top, Under top,
    private let pagePaddingPrefrences = [25, 25, 15, 0]
    private var animatePresentation: Bool!
    private var userCheckerBool: Bool!
    
    @IBOutlet var topConstraintForTitleImage: NSLayoutConstraint!
    @IBOutlet var titleImage: UIImageView!
    
    var onboardingViewController: OnboardingViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        
        //Loads onboarding
        self.userInitialLogin = self.userData.retreiveUserInitialLoginStatus()
        self.userCheckerBool = false

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.titleImageAnimation()
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.userData.saveUserInitalLogin(complete: true)
        self.animatePresentation = true
        self.pages = []
    }
    
    //Handles animation from launchscreen into help screen
    func titleImageAnimation() {
        
        self.topConstraintForTitleImage.constant = 1
        if self.userCheckerBool == true {
            self.titleImage.alpha = 0.0
        }
        UIView.animate(withDuration: 0.8, animations: {
            self.view.layoutIfNeeded()
        }) { (true) in
            if self.userCheckerBool == false {
                self.animatePresentation = false
                self.userCheckerBool = true
                self.presentOnboarding()
            } else {
                self.animatePresentation = true
                self.presentOnboarding()
            }
            
        }
        if self.userInitialLogin == false {
            if(self.mapViewSharedInstance.locationManager.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization))) {
                self.mapViewSharedInstance.locationManager.requestWhenInUseAuthorization()
            }
        }
    }
    
    func presentOnboarding() -> Void {

        let secondPage = OnboardingContentViewController(title: "Welcome To Antenna Point!", body: "Antenna Point will help you find the best direction to face your Antennas Direct antenna.This application will work for setting up both indoor and outdoor antennas.", image: UIImage(named:"LargerAntennaPoint Logo"), buttonText: "Next") {
            self.onboardingViewController.moveNextPage()
        }
        self.pages.append(secondPage)
        
        let thirdPage = OnboardingContentViewController(title: "Getting started", body: "For best results be sure to hold your device flat in your hand and turn slowly as well as make sure you are at least 5 feet from any electrical devices that have a magnetic feild, such as a computer or TV. These devices can cause the application to inacuratley track your current heading.", image: UIImage(named: "LargerAntennaPoint Logo"), buttonText: "Next") {
            self.onboardingViewController.moveNextPage()
            /*
            self.dismiss(animated: true, completion: nil)
            self.tabBarController?.selectedIndex = 1
 */
        }
        self.pages.append(thirdPage)
        
        let fourthPage = OnboardingContentViewController(title: "Basics", body: "Antenna Point tracks your location as well as the direction you are facing. This can be seen on the main screen shown above.", image: UIImage(named: "HelpPageUserLocation"), buttonText: "Next") {
            self.onboardingViewController.moveNextPage()
        }
        self.pages.append(fourthPage)
        
        let fifthPage = OnboardingContentViewController(title: "Functions", body: "The top left 'Crosshair' button will display a vector showing you the best general direction to face your antenna. You can tap on an individual tower icon to see its information.", image: UIImage(named: "HelpPageSelectOverlay"), buttonText: "Next") {
            self.onboardingViewController.moveNextPage()
        }
        self.pages.append(fifthPage)
        
        let sixthPage = OnboardingContentViewController(title: "Correct Placment", body: "When ready select the 'Crosshair' icon and an overlay like show above will appear on the map. Rotate your device to allign your heading with the overlay as show above.", image: UIImage(named: "HelpPageVectorOverlay"), buttonText: "Next") {
            self.onboardingViewController.moveNextPage()
        }
        self.pages.append(sixthPage)
        
        let finalPage = OnboardingContentViewController(title: "Place Antenna", body: "Once you have lined up with the overlay you have found the best general direction to face your antenna! Place your Antennas Direct antenna and start enjoying your free channels!", image: UIImage(named:"LargerAntennaPoint Logo"), buttonText: "Got it!") {
            self.dismiss(animated: true, completion: nil)
            self.tabBarController?.selectedIndex = 1
        }
        self.pages.append(finalPage)
        
        //Set page prefrences
        for page in self.pages {
            page.topPadding = CGFloat(self.pagePaddingPrefrences[0])
            page.underIconPadding = CGFloat(self.pagePaddingPrefrences[1])
            page.underTitlePadding = CGFloat(self.pagePaddingPrefrences[2])
            page.actionButton.adjustsImageWhenHighlighted = true
            page.actionButton.setTitleColor(UIColor.blue, for: UIControlState.normal)
            page.titleLabel.textColor = UIColor.black
            page.bodyLabel.textColor = UIColor.black
            print(page.bodyLabel.font)
            
            if self.deviceChecker == "iPhone 5" || self.deviceChecker == "iPhone 5c" || self.deviceChecker == "iPhone 5s" || self.deviceChecker == "iPhone 4" || self.deviceChecker == "iPhone 4s" {
                page.bodyLabel.font = UIFont(name: "Helvetica-Light", size: 20)
                page.underTitlePadding = CGFloat(self.pagePaddingPrefrences[2])
            }else {
                page.bodyLabel.font = UIFont(name: "Helvetica-Light", size: 25)
            }
        }

        self.onboardingViewController = OnboardingViewController(backgroundImage: UIImage(named: "PlainWhiteImage"), contents: self.pages)
        onboardingViewController?.shouldMaskBackground = false
        self.onboardingViewController?.pageControl.currentPageIndicatorTintColor = UIColor.blue
        self.onboardingViewController?.pageControl.pageIndicatorTintColor = UIColor.gray
        
        self.present(self.onboardingViewController!, animated: self.animatePresentation, completion: nil)
  
    }
}

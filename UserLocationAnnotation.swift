//
//  CustomUserLocationAnnotation.swift
//  Antenna Point
//
//  Created by Timothy Aven on 4/10/17.
//  Copyright © 2017 Antennas Direct. All rights reserved.
//

import Foundation
import MapKit

//Class for custom user location annoation
class UserLocationAnnoation: NSObject, MKAnnotation {
    
    //User location coordinate
    var coordinate: CLLocationCoordinate2D
    
    init(coordinate: CLLocationCoordinate2D) {
        
        self.coordinate = coordinate
        super.init()
    }
    
}

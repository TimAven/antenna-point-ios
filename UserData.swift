//
//  UserData.swift
//  Antenna Point
//
//  Created by Timothy Aven on 10/17/16.
//  Copyright © 2016 Antennas Direct. All rights reserved.
//

import Foundation

class UserData : NSObject {
    
    static let sharedInstance = UserData()
    let defaults = UserDefaults.standard
    
    //Keys
    let kUserInitalLogin = "initalLogin"
    let kUserLocationAccess = "locationAccess"
 
    //Save functions
    func saveUserInitalLogin(complete: Bool) -> () {
        self.defaults.set(complete, forKey: self.kUserInitalLogin)
    }
    func saveUserLocationAccess(complete: Bool) -> () {
        self.defaults.set(complete, forKey: self.kUserLocationAccess)
    }

    //Retreive functions
    func retreiveUserInitialLoginStatus() -> Bool {
        return (self.defaults.object(forKey: self.kUserInitalLogin) != nil)
    }
    func retreiveUserLocationAccess() -> Bool {
        return (self.defaults.object(forKey: self.kUserLocationAccess) != nil)
    }

}
